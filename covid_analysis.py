import pandas
import matplotlib.pyplot as plt
import datetime as dt
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Lasso
from sklearn.model_selection import KFold

def extract_covid_data():
    """
    Extracts the data from the csv file.

    Input: None
    Output: Dataframe containing the information within the csv file.
    """
    base_filepath = "/home/conan/Documents/Projects/COVID-19_County_Data"
    filename = "us-counties.csv"
    
    data_filepath = base_filepath + "/" + filename
    return  pandas.read_csv(data_filepath,
                            parse_dates = ['date'])

def get_county_data(dataframe, county, state):
    """
    Filters out the input dataframe and returns a dataframe only for that county.

    Input: 
        Dataframe - Dataframe of data.
        County - County as a string 
    """
    print( dataframe['state']==state)
    state_data = dataframe[dataframe['state']==state]
    print(state_data)
    county_data = state_data[state_data['county']==county]
    return county_data

def main():
    # Get COVID-19 data
    covid_dataframe = extract_covid_data()
    dane_county_data = get_county_data(covid_dataframe,'Dane','Wisconsin')
    print(dane_county_data.tail(20))
    dane_covid_change = dane_county_data['cases'].diff()

    # Plot data for Dane county
    dane_covid_cases_plot = plt.plot(dane_county_data['date'],dane_county_data['cases'], label='COVID-19 Cases')
    plt.xticks(rotation=45)
    dane_covid_deaths_plot = plt.plot(dane_county_data['date'],dane_county_data['deaths'], label='COVID-19 Deaths')
    plt.legend()
    plt.title('COVID-19 Cases and Deaths in Dane County')

    # Add beginning and end of stay at home order
    plt.axvline(dt.datetime(2020,3,12))
    plt.text(dt.datetime(2020,3,13),30,'Start of Stay at Home Order', rotation=90)
    plt.axvline(dt.datetime(2020,5,13))
    plt.text(dt.datetime(2020,5,14),0,'End of Stay at Home Order',rotation=90)
    plt.axvline(dt.datetime(2020,7,13))
    plt.text(dt.datetime(2020,7,14),0,'Dane County Mask Mandate',rotation=90)
    plt.axvline(dt.datetime(2020,9,7))
    plt.text(dt.datetime(2020,9,8),0,'UW School Begins', rotation=90)
    plt.axvline(dt.datetime(2020,8,25))
    plt.text(dt.datetime(2020,8,26),0,'UW Move-in Start', rotation=90)
    plt.show()
    plt.savefig('/home/conan/Pictures/COVID-19/dane_county_covid.png')

    # Plot differences in cases
    plt.figure(1)
    plt.xticks(rotation=45)
    dane_covid_cases_delta = plt.plot(dane_county_data['date'],dane_covid_change, label='COVID-19 New Cases')
    plt.legend()
    plt.axvline(dt.datetime(2020,3,12))
    plt.text(dt.datetime(2020,3,13),30,'Start of Stay at Home Order', rotation=90)
    plt.axvline(dt.datetime(2020,5,13))
    plt.text(dt.datetime(2020,5,14),0,'End of Stay at Home Order',rotation=90)
    plt.axvline(dt.datetime(2020,7,13))
    plt.text(dt.datetime(2020,7,14),0,'Dane County Mask Mandate',rotation=90)
    plt.axvline(dt.datetime(2020,9,7))
    plt.text(dt.datetime(2020,9,8),0,'UW School Begins', rotation=90)
    plt.axvline(dt.datetime(2020,8,25))
    plt.text(dt.datetime(2020,8,26),0,'UW Move-in Start', rotation=90)
    plt.title('New COVID Cases')
    plt.show()

    # Apply regular least squares
    float_time = [[i.timestamp()] for i in dane_county_data['date']]
    float_time = float_time[139:]
    dane_covid_cases = [i for i in dane_county_data['cases']]
    dane_covid_cases = dane_covid_cases[139:]
    reg = LinearRegression().fit(float_time,dane_covid_cases)
    print("Least Squares:",reg.score(float_time,dane_covid_cases))
    print("Coefficients:",reg.coef_)
    print("Intercept:",reg.intercept_)

if __name__ == "__main__":
    main()
