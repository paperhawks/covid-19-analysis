#!/bin/bash

basedir='/home/conan/Documents/Projects/COVID-19_County_Data'
cd $basedir
wget https://opendata.arcgis.com/datasets/9d0cb9329d5745cfbf6ce91fa9835c6e_1.csv
mv -i $basedir/9d0cb9329d5745cfbf6ce91fa9835c6e_1.csv $basedir/Dane_DOH_County_Data/COVID-19_Data_by_County_$(date +%m%d --date="1 day ago").csv
