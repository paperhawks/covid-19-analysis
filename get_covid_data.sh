#! /bin/bash

# make backup
mv /home/conan/Documents/Projects/COVID-19_County_Data/us-counties.csv /home/conan/Documents/Projects/COVID-19_County_Data/Backup/us-counties.csv.bak_$(date +%F)

#get data
wget https://raw.githubusercontent.com/nytimes/covid-19-data/master/us-counties.csv
